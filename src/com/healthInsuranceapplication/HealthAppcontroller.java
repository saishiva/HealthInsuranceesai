package com.healthInsuranceapplication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HealthAppcontroller extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String age = request.getParameter("age");
		String gender = request.getParameter("gender");
		String Hypertension = request.getParameter("Hypertension");
		String bloodpressure = request.getParameter("Bloodpressure");
		String bloodsugar = request.getParameter("Bloodsugar");
		String Overweight = request.getParameter("Overweight");
		String Smoking = request.getParameter("Smoking");
		String alcohol = request.getParameter("Alcohol");
		String exercise = request.getParameter("exercise");
		String drugs = request.getParameter("Drugs");
		
		HelathBean hbean=new HelathBean();
		hbean.setAge(age);
		hbean.setName(name);
		hbean.setGender(gender);
		hbean.setAlcohol(alcohol);
		hbean.setBloodpressure(bloodpressure);
		hbean.setBloodsugar(bloodsugar);
		hbean.setDrugs(drugs);
		hbean.setExercise(exercise);
		HealthService healthservice=new HealthService();
		
		HelathBean hbean1=healthservice.healthService(hbean);
		
		
		
		
		
		
		
		if(name.isEmpty()||gender.isEmpty()||age.isEmpty())
		{
			RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
			out.println("<font color=red>Please fill all the fields</font>");
			rd.include(request, response);
		}
		else
		{
			RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
			rd.forward(request, response);
		}
	}

}
